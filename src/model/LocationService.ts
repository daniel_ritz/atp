import { type Ref, ref, onMounted, onUnmounted } from "vue";

export enum LocationState {
    Inactive,
    Deactivated,
    Unavailable,
    Ready
}

export const state = ref(LocationState.Inactive)
export const currentLocation = <Ref<GeolocationCoordinates|undefined>>ref(undefined)

var watcher = <number|null>null

export function activateLocation(): Promise<GeolocationCoordinates> {
    return new Promise((resolve, reject) => {
        if (!navigator.geolocation) {
            state.value = LocationState.Unavailable
            reject(state.value)
            return
        }

        navigator.geolocation.getCurrentPosition(pos => {
            handleNewLocation(pos);
            initWatcher()
            resolve(pos.coords)
        }, err => {
            handleError(err);
            reject(state.value)
        })

    })    
}

function handleNewLocation(pos: GeolocationPosition) {
    state.value = LocationState.Ready;
    currentLocation.value = pos.coords;
}

function handleError(err: GeolocationPositionError) {
    if (err.code == err.POSITION_UNAVAILABLE) {
        state.value = LocationState.Unavailable;
    } else if (err.code == err.PERMISSION_DENIED) {
        state.value = LocationState.Deactivated;
    }
}

function initWatcher() {
    watcher = navigator.geolocation.watchPosition(handleNewLocation, handleError)
}

export function deactivateLocation() {
    state.value = LocationState.Inactive
    if (watcher != null) navigator.geolocation.clearWatch(watcher)
}

export async function autoEnable(): Promise<GeolocationCoordinates|undefined> {
    if (state.value == LocationState.Ready) return currentLocation.value
    if (!navigator.permissions) return undefined
    const permission = await navigator.permissions.query({name: 'geolocation'})
    if (permission.state == 'granted') {
        return await activateLocation()
    }
}