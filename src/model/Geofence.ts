import { distanceTo, type LatLng } from "geolocation-utils"
import { watch, type WatchStopHandle } from "vue"
import { currentLocation } from "./LocationService"

export default class Geofence {
    private radius: number
    private lastLocation: LatLng|undefined = undefined
    private callback: () => void
    private stopWatcher: WatchStopHandle|undefined = undefined

    constructor(radius: number, callback: () => void) {
        this.radius = radius
        this.callback = callback
    }

    start() {
        if (this.stopWatcher != undefined) return
        this.stopWatcher = watch(currentLocation, coords => {
            if (coords == undefined) return
            const newLocation = <LatLng>{lat: coords!.latitude, lng: coords!.longitude}
            if (this.lastLocation == undefined) {
                this.lastLocation = newLocation
                return
            }
            const distance = distanceTo(this.lastLocation, newLocation)
            if (distance > this.radius) {
                this.lastLocation = newLocation
                this.callback()
            }
        })
    }

    stop() {
        if (this.stopWatcher) this.stopWatcher()
        this.stopWatcher = undefined
    }


}