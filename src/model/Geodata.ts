
const endpoint = 'https://overpass-api.de/api/interpreter'

function timeoutSignal(ms: number) {
    const controller = new AbortController()
    setTimeout(() => controller.abort(), ms)
    return controller.signal
}

export enum RequestState {
    Ready,
    Sent,
    Deferred,
    Timeout,
    Finished,
    Aborted,
}

export class ApiRequest {
    private payload: string
    private retries: number
    private successCallback: (result: any, cancelled: boolean) => void
    private errorCallback: (err: any) => void
    private cancelled = false
    public stateChanged = (state: RequestState) => {}
    public state = RequestState.Ready

    constructor(payload: string, success: (result: any, cancelled: boolean) => void, error = (_: any) => {}, retries = 3) {
        this.payload = payload
        this.retries = retries
        this.successCallback = success
        this.errorCallback = error
        this.setState(RequestState.Ready)
    }

    private setState(state: RequestState) {
        this.state = state
        this.stateChanged(state)
    }

    async send() {

        const command = encodeURIComponent(this.payload)
        try {
            this.setState(RequestState.Sent)
	        const response = await fetch(endpoint, {
	            method: "POST",
	            body: `data=${command}`,
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
	            },
	            signal: timeoutSignal(10_000),
	        })
            if (response.ok) {
	            const json = await response.json()
	            this.successCallback(json, this.cancelled)
                this.setState(RequestState.Finished)
            } else if (response.status == 431 || response.status == 502) {
                this.setState(RequestState.Deferred)
                setTimeout(() => this.retry, 10_000)
            }
        } catch (err: any) {
            if (err.name === "TimeoutError") {
                this.setState(RequestState.Timeout)
                this.retry()
            } else if (err.name === "AbortError") {
                // user cancelled
                this.setState(RequestState.Aborted)
                this.errorCallback(err)
            } else {
                console.error(err)
                this.setState(RequestState.Aborted)
                this.errorCallback(err)
            }
        }

    }

    cancel() {
        this.cancelled = true
    }

    private retry() {
        if (!this.cancelled || this.retries-- > 0) {
            console.log('retry')
            this.send()
        } else {
            this.setState(RequestState.Aborted)
            this.errorCallback("aborted")
        }
    }

}

export class SingleRequestManager {
    private requests: ApiRequest[] = []

    submit(request: ApiRequest) {
        this.requests.forEach(r => {r.cancel()});
        this.cleanup()
        this.requests.push(request)
        request.send()
    }

    private cleanup() {
        while (this.requests.length > 0 && [RequestState.Finished, RequestState.Aborted].includes(this.requests[0].state)) {
            this.requests.splice(0, 1)
        }
    }

}