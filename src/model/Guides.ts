export interface GuidedTour {
    title: string,
    description: string,
    category: string[],
    command: string,
}

const guides = <GuidedTour[]>[
    {
        title: "Statues",
        description: "Statues in this area",
        category: ["Art"],
        command: `[out:json];
        (
        node[artwork_type=statue]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Sculptures",
        description: "Sculptures in this area",
        category: ["Art"],
        command: `[out:json];
        (
        node[artwork_type=sculpture]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Murals",
        description: "Murals in this area",
        category: ["Art"],
        command: `[out:json];
        (
        node[artwork_type=mural]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Architecture",
        description: "Notable architecture in this area",
        category: ["Architecture"],
        command: `[out:json];
        (
        node[artwork_type=architecture]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Obelisks",
        description: "Obelisks in this area",
        category: ["Architecture", "Memorial"],
        command: `[out:json];
        (
        node[memorial=obelisk]({BBOX});
        node["man_made"=obelisk]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Memorials",
        description: "Memorials in this area",
        category: ["Memorial"],
        command: `[out:json];
        (
        node[memorial]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Stolpersteine",
        description: "A worldwide memorial installation commemorating the victims of the nazi regime",
        category: ["Memorial"],
        command: `[out:json];
        (
        node[memorial=stolperstein]({BBOX});
        node["memorial:type"=stolperstein]({BBOX});
        );
        out body;
        >;`,
    },
    {
        title: "Platz der Grundrechte",
        description: "Decentral installation in Karlsruhe",
        category: ["Memorial", "Art"],
        command: `[out:json];
        (
        node[tourism=artwork][name="Platz der Grundrechte"]({BBOX});
        );
        out body;
        >;`,
    }
]

export function getGuides() {
    return guides
}