import { distanceTo, getBoundingBox } from 'geolocation-utils'
import { ApiRequest } from './Geodata'

export function findNearbyPOIs(coords: GeolocationCoordinates, bounds: any | null = null): Promise<any[]> {

    return new Promise((resolve, reject) => {

        bounds = bounds ?? <any>getBoundingBox([{lat: coords.latitude, lon: coords.longitude}], 200)

        const bbox = `${bounds.bottomRight.lat},${bounds.topLeft.lon},${bounds.topLeft.lat},${bounds.bottomRight.lon}`
        const command = `[out:json];
        (
        node[tourism=artwork](${bbox});
        node[tourism=attraction](${bbox});
        node[tourism=gallery](${bbox});
        node[tourism=museum](${bbox});
        node[tourism=viewpoint](${bbox});
        node[historic](${bbox});
        );
        out body;
        >;`

        const request = new ApiRequest(command, result => {
            resolve(<any[]>result.elements)
        }, reject)
        request.send()

    })

    
}

export function getCity(bounds: any): Promise<any> {
    return new Promise((resolve, reject) => {

        const bbox = `${bounds.bottomRight.lat},${bounds.topLeft.lon},${bounds.topLeft.lat},${bounds.bottomRight.lon}`
        const command = `[out:json];
        (
            node[place=city](${bbox});
            node[place=town](${bbox});
            node[place=village](${bbox});
            node[place=suburb](${bbox});
            node[place=borough](${bbox});
            node[place=quarter](${bbox});
            node[place=neighbourhood](${bbox});
        );
        out body;
        >;`

        const request = new ApiRequest(command, result => {
            resolve(<any[]>result.elements[0]?.tags.name ?? null)
        }, reject)
        request.send()

    })
    
}

export function findPOIsByCommand(command: string, coords: GeolocationCoordinates, bounds: any): Promise<any[]> {

    return new Promise((resolve, reject) => {

        const bbox = `${bounds.bottomRight.lat},${bounds.topLeft.lon},${bounds.topLeft.lat},${bounds.bottomRight.lon}`
        command = command.replace(/{BBOX}/g, bbox)
        const request = new ApiRequest(command, result => {
            resolve(<any[]>result.elements)
        }, reject)
        request.send()

    })

}

export function annotateDistance(elements: any[], coords: GeolocationCoordinates) {
    return elements.map(element => {
        const dist = distanceTo(element, {lat: coords.latitude, lon: coords.longitude})
        element.distance = Math.floor(dist)
        return element
    })
    
}